import { createApp } from "vue";
import { createPinia } from "pinia";

import Grossairvo from "./Grossairvo.vue";
import router from "./router";

const grossairvo = createApp(Grossairvo);

grossairvo.use(createPinia());
grossairvo.use(router);

grossairvo.mount("#app");
